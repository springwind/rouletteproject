using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 세팅 패널을 나타내기 위한 컴포넌트
/// </summary>
public class SettingPanel : MonoBehaviour
{
    [Header("룰렛 게임 객체")]
    public RouletteGame m_RouletteGame;

    /// <summary>
    /// 비우기 버튼을 나타냅니다.
    /// </summary>
    [Header("# 비우기 버튼")]
    public Button m_ClearButton;

    /// <summary>
    /// 시작하기 버튼을 나타냅니다.
    /// </summary>
    [Header("# 시작하기 버튼")]
    public Button m_StartButton;

    /// <summary>
    /// 항목을 입력받을 InputField 컴포넌트들을 나타냅니다.
    /// </summary>
    [Header("# 입력 필드")]
    public TMP_InputField[] m_InputFields;

    private void Awake()
    {
        // 버튼 이벤트 설정
        m_ClearButton.onClick.AddListener(OnClearButtonClicked);
        m_StartButton.onClick.AddListener(OnStartButtonClicked);

    }
    /// <summary>
    /// 비우기 버튼 클릭시 호출되는 메서드입니다
    /// Awake 에서 버튼 이벤트로 바인딩 되었습니다.
    /// </summary>
    private void OnClearButtonClicked()
    {
        foreach(TMP_InputField inputField in m_InputFields)
        {
            inputField.text = null;
        }
    }
    /// <summary>
    /// 시작하기 버튼 클릭 시 호출되는 메서드입니다.
    /// Awake 에서 버튼 이벤트로 바인딩 되었습니다.
    /// </summary>
    private void OnStartButtonClicked()
    {
        // 입력 필드에 담긴 문자열들을 얻습니다.
        string[] itemStrings = GetInpuFieldToArray();

        // 룰렛 게임 시작
        m_RouletteGame.OnStartGame(itemStrings);

        
    }
    /// <summary>
    /// 입력 필드의 텍스트를 배열로 반환합니다.
    /// </summary>
    /// <returns>문자열을 배열에 담아 반환합니다.</returns>
    private string[] GetInpuFieldToArray()
    {
        // 반환시킬 문자열이 저장될 리스트
        List<string> itemStrings = new();

        foreach(TMP_InputField inputField in m_InputFields)
        {
            // 리스트 요소에 설정된 내용을 추가합니다.
            itemStrings.Add(inputField.text);
        }
        // 리슽트의 내용을 배열로 변환하여 반환합니다.
        return itemStrings.ToArray();
    }

}
